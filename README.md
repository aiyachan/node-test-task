# A node.js app with deployment example


## Description

This app outputs "Hello World" on 3000 port

## Not finished, but planned

Inside of deployment, there is a folder, named as kube-cluster. I tried to build a working kube cluster, use podman-compose to hold 1 master and 3 workers node. The plan was to deploy an app on this cluster. Unfortunately, there are many problems in using the kubespray to make a cluster inside of podman containers, so I decided to do it when I will have more time.

## Authors
Made by sferatime as a test task 5 of Bamboo
